const child_process = require('child_process')

const logger = function(prefix = '', cb) {
  return function(data) {
    process.stdout.write(`${data}`)
    cb && cb(data)
  }
}

/* Redis */
const docker = child_process.spawn('yarn', ['run', 'start:docker:test'])

docker.stdout.on('data', logger())

docker.stderr.on('data', logger())

docker.on('error', logger())

docker.on(
  'close',
  logger('', code => {
    process.exit(code)
  })
)

/* Server */
let server

setTimeout(() => {
  server = child_process.spawn('yarn', ['run', 'test:run'])

  server.stdout.on('data', logger())

  server.stderr.on('data', logger())

  server.on('error', logger())

  server.on(
    'close',
    logger('[SERVER] exited with code', code => {
      docker.kill('SIGINT')
      process.exit(code)
    })
  )
}, 2000)

const exitHandler = function(err) {
  console.log('Error before exit: ' + err)
  try {
    docker.kill('SIGINT')
    server.kill('SIGINT')
  } catch (e) {}
}

// Do something when app is closing
process.on('exit', exitHandler.bind(null))
process.on('SIGINT', exitHandler.bind(null))
process.on('SIGQUIT', exitHandler.bind(null))
process.on('uncaughtException', exitHandler.bind(null))
