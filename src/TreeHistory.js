// Dependencies
import _isPlainObject from 'lodash-es/isPlainObject'

import { Mixin } from 'mixwith/src/mixwith'

// Constants
const MAX_LENGTH = 0x20
const MAX_LENGTH_LESS_ONE = MAX_LENGTH - 1 // This micro-optimization only works if MAX_LENGTH is a power of two

/**
  * TreeHistory
  */
const TreeHistory = Mixin(
  superclass =>
    class extends superclass {
      constructor(args) {
        super(args)

        Object.defineProperty(this, '_history', { value: {} })
      }

      onchange(key, next, prev, once) {
        super.onchange(key, next, prev, once)

        if (_isPlainObject(next) || _isPlainObject(prev)) {
          return
        }

        if (this._history[key] == null) {
          this._history[key] = new TreeHistoryLog(this, key)
        }

        this._history[key].shift(next)
      }
    }
)

class TreeHistoryLog {
  constructor(tree, key) {
    this.tree = tree
    this.key = key
    this.values = []
    this.times = []
    this.pointer = 0
  }

  shift(value) {
    this.values[this.pointer] = value
    this.times[this.pointer] = Date.now()
    this.pointer = (this.pointer + 1) & MAX_LENGTH_LESS_ONE // A micro-optimization; universally, use `(this.pointer + 1) % MAX_LENGTH`

    if (this.pointer === 0) {
      this.save(this.read())
    }
  }

  read() {
    if (this.pointer === 0) {
      return {
        values: this.values.slice(),
        times: this.times.slice()
      }
    }

    return {
      values: this.values
        .slice(this.pointer, MAX_LENGTH)
        .concat(this.values.slice(0, this.pointer)),
      times: this.times
        .slice(this.pointer, MAX_LENGTH)
        .concat(this.times.slice(0, this.pointer))
    }
  }

  save() {}
}

export default TreeHistory
