// Dependencies
import _each from 'lodash-es/each'
import _keys from 'lodash-es/keys'
import _omit from 'lodash-es/omit'
import _isArray from 'lodash-es/isArray'
import _isBoolean from 'lodash-es/isBoolean'
import _isEmpty from 'lodash-es/isEmpty'
import _isNumber from 'lodash-es/isNumber'
import _isNull from 'lodash-es/isNull'
import _isPlainObject from 'lodash-es/isPlainObject'
import _isString from 'lodash-es/isString'
import _isUndefined from 'lodash-es/isUndefined'

import _toJsonPointer from './lib/toJsonPointer'

/**
  * TreeBase
  */
class TreeBase {
  constructor({ parent, key, namespace }) {
    const proxy = new Proxy(this, {
      set: (target, key, value) => {
        let prev, next

        prev = target[key]
        next = value

        // Validate Key
        switch (true) {
          case _isString(prev):
          case _isNumber(prev):
          case _isBoolean(prev):
          case _isNull(prev):
          case _isUndefined(prev):
          case _isPlainObject(prev):
          case _isArray(prev):
          case prev instanceof this.constructor:
            break
          default:
            throw new TypeError(
              `Cannot overwrite existing method or property { ${key}: ${prev} }`
            )
        }

        switch (true) {
          case _isString(value):
          case _isNumber(value):
          case _isBoolean(value):
          case _isNull(value):
          case _isArray(value):
            if (prev === next) {
              return true
            }

            if (prev instanceof this.constructor) {
              _each(prev, function(ignored, key) {
                prev[key] = undefined
              })
            }

            target[key] = value
            break
          case _isUndefined(value):
            if (prev instanceof this.constructor) {
              _each(prev, function(ignored, key) {
                delete prev[key]
              })
            }
            delete target[key]
            if (_isEmpty(target)) {
              if (target.parent != null) {
                target.parent[target.key] = undefined
              }
            }
            break
          case _isPlainObject(value):
            if (prev instanceof this.constructor) {
              ;(function(target, next, prev) {
                const removed = _omit(prev, _keys(next))

                _each(removed, function(value, key) {
                  target[key] = undefined
                  delete target[key]
                })

                _each(next, function(value, key) {
                  target[key] = value
                })
              })(target[key], value, prev)
            } else {
              target[key] = new this.constructor({
                parent: proxy,
                key
              })

              target[key].init(value)
            }
            return true
          default:
            return false
          /* TODO: Add switch to support throwing error or logging error.
            throw new TypeError(`${JSON.stringify(value)} must be a primitive`)
          */
        }

        proxy.onchange(key, next, prev)

        return true
      },
      deleteProperty: (target, key) => {
        const prev = target[key]

        if (prev instanceof this.constructor) {
          _each(prev, function(ignored, key) {
            delete prev[key]
          })
        }

        delete target[key]
        proxy.onchange(key, undefined, prev, true)

        return true
      }
    })

    if (parent == null) {
      Object.defineProperties(proxy, {
        _namespace: {
          value: namespace == null ? '' : namespace
        },
        _root: {
          value: proxy
        },
        _parent: {
          value: null
        },
        _key: {
          value: null
        },
        _jsonPointer: {
          value: _toJsonPointer('')
        },
        _path: {
          get() {
            return this._jsonPointer.path
          }
        },
        _pointer: {
          get() {
            return this._jsonPointer.pointer
          }
        }
      })
    } else {
      Object.defineProperties(proxy, {
        _namespace: {
          value: parent._namespace
        },
        _root: {
          value: parent._root
        },
        _parent: {
          value: parent
        },
        _key: {
          value: key
        },
        _jsonPointer: {
          value: _toJsonPointer(parent._pointer + '/' + key)
        },
        _path: {
          get() {
            return this._jsonPointer.path
          }
        },
        _pointer: {
          get() {
            return this._jsonPointer.pointer
          }
        }
      })
    }

    return proxy
  }

  onchange() {}
}

export default TreeBase
