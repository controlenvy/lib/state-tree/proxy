// Dependencies
import _isFunction from 'lodash-es/isFunction'
import _isUndefined from 'lodash-es/isUndefined'
import _setWith from 'lodash-es/setWith'

import Redis from 'redis'

import { Mixin } from 'mixwith/src/mixwith'
import _toJsonPointer from './lib/toJsonPointer'

// Redis client
const redis = Redis.createClient()

/**
  * TreeRedis
  */
const TreeRedis = Mixin(
  superclass =>
    class extends superclass {
      constructor(args) {
        super(args)

        Object.defineProperties(this, {
          _redis: {
            value: redis
          }
        })

        if (this._parent != null) {
          return
        }

        this._redis.hgetall(this._namespace, (error, reply) => {
          if (error != null) {
            // console.error(error)
            return
          }

          for (let key in reply) {
            let value = reply[key]
            let pointer = _toJsonPointer(key)

            _setWith(this, pointer.path, JSON.parse(value), Object)
          }

          this.constructor.forest.set(this._namespace, this)

          if (_isFunction(args.done)) {
            args.done.call(this)
          }
        })
      }

      onchange(key, next, prev, once) {
        super.onchange(key, next, prev, once)

        const pointer = _toJsonPointer(this._pointer + '/' + key).pointer
        if (_isUndefined(next)) {
          this._redis.hdel(this._namespace, pointer)
        } else {
          this._redis.hset(this._namespace, pointer, JSON.stringify(next))
        }
      }
    }
)

export default TreeRedis
