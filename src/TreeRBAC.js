// Dependencies
import _assign from 'lodash-es/assign'
import _get from 'lodash-es/get'
import _isEmpty from 'lodash-es/isEmpty'
import _merge from 'lodash-es/merge'
import _reduce from 'lodash-es/reduce'
import _setWith from 'lodash-es/setWith'
import _forEach from 'lodash-es/forEach'
import _difference from 'lodash-es/difference'
import _keys from 'lodash-es/keys'

import { Mixin } from 'mixwith/src/mixwith'
import _permit from './lib/permit'

/**
  * TreeRBAC
  */
const TreeRBAC = Mixin(
  superclass =>
    class extends superclass {
      permit(roles) {
        return _reduce(
          this,
          (result, value, key) => {
            if (value instanceof this.constructor) {
              const permitted = value.permit(roles)
              if (!_isEmpty(permitted)) {
                result[key] = permitted
              }
            } else {
              if (_permit(this._path.concat(key), roles)) {
                result[key] = value
              }
            }

            return result
          },
          {}
        )
      }

      read(path, roles) {
        path = this.parent != null ? this._path.concat(path) : path
        if (!_permit(path, roles)) {
          return
        }

        const value = path.length > 0 ? _get(this, path) : this
        if (value instanceof this.constructor) {
          return value.permit(roles)
        } else {
          return value
        }
      }

      write(path, value, roles) {
        path = this.parent != null ? this._path.concat(path) : path
        if (!_permit(path, roles)) {
          return false
        }

        if (path.length > 0) {
          _setWith(this, path, value, Object)
        } else {
          const removing = _difference(_keys(this), _keys(value))
          _forEach(removing, key => {
            this[key] = undefined
          })
          _assign(this, value)
        }

        return true
      }

      merge(path, value, roles) {
        path = this.parent != null ? this._path.concat(path) : path
        if (!_permit(path, roles)) {
          return false
        }

        const tree = path.length > 0 ? _get(this, path) : this

        if (tree == null || !(tree instanceof this.constructor)) {
          _setWith(this, path, value, Object)
        } else {
          _merge(tree, value)
        }

        return true
      }
    }
)

export default TreeRBAC
