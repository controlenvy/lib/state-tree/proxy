// Dependencies
import _get from 'lodash-es/get'
import _isArray from 'lodash-es/isArray'
import _isPlainObject from 'lodash-es/isPlainObject'
import _take from 'lodash-es/take'
import _takeRight from 'lodash-es/takeRight'

import { Mixin } from 'mixwith/src/mixwith'
// import _toJsonPointer from './lib/toJsonPointer'

/**
  * TreeEvents
  */
const TreeEvents = Mixin(
  superclass =>
    class extends superclass {
      constructor(args) {
        super(args)

        const events =
          this._parent == null ? new TreeEventsBroker() : this._parent._events

        Object.defineProperty(this, '_events', { value: events })
      }

      onchange(key, next, prev, once) {
        super.onchange(key, next, prev, once)

        if (_isPlainObject(next) || _isPlainObject(prev)) {
          return
        }

        this._events.publish(this, key, next, once)
      }

      subscribe({ path, subscriber, callback, immediate }) {
        path = this._path.concat(path)

        const event = new TreeEvent({
          root: this._root,
          path,
          subscriber,
          callback
        })

        if (immediate === true) {
          event.publish()
        }
      }

      unsubscribe({ path, subscriber }) {
        if (path != null) {
          path = this._path.concat(path)

          const key = JSON.stringify(path)

          this._events.unsubscribe({ subscriber, key })
        } else {
          const listeners = this._events.SubscriberNestedMap.get([subscriber])

          if (listeners == null) {
            return
          }

          listeners.forEach((listener, key) => {
            this._events.PointerMultiMap.delete(key, listener)
          })

          this._events.SubscriberNestedMap.delete([subscriber])
        }
      }
    }
)

class TreeEvent {
  constructor({ root, path, subscriber, callback, index }) {
    this.root = root
    this.subscriber = subscriber
    this.index = index
    this.key = JSON.stringify(path)

    if (callback == null) {
      this.callback = (root, left, right, next) => {
        subscriber.recomputePath({ index: this.index, value: next })
        subscriber.publish()
      }
    } else {
      this.callback = callback
    }

    this.path = path.map((step, index) => {
      if (_isArray(step)) {
        const event = new TreeEvent({
          root: this.root,
          path: step,
          subscriber: this,
          index
        })

        if (!_isArray(this.children)) {
          this.children = []
        }
        this.children.push(event)

        return _get(this.root, event.path)
      }

      return step
    })

    this.root._events.subscribe({ event: this })
  }

  toString() {
    return `[TreeEvent '${this.key}']`
  }

  recomputePath({ index, value }) {
    this.path[index] = value

    this.root._events.subscribe({ event: this })
  }

  publish() {
    const value = _get(this.root, this.path)

    this.callback(this.root, this.path, [], value)
  }
}

class TreeEventsBroker {
  constructor() {
    this.SubscriberNestedMap = new NestedMap()
    this.PointerMultiMap = new MultiMap()
  }

  toString() {
    return '[object TreeEventsBroker]'
  }

  publish(target, key, next, once = false) {
    const tree = target._root
    const path = target._path.concat(key)

    for (let i = path.length; i >= 0; i--) {
      let pathLeft = _take(path, i)
      let pathRight = _takeRight(path, path.length - i)
      let pointer = JSON.stringify(pathLeft)

      let set = this.PointerMultiMap.get(pointer)
      if (set instanceof Set) {
        set.forEach(function(event) {
          event.callback(tree, pathLeft, pathRight, next)
        })
      }

      if (once) {
        break
      }
    }
  }

  subscribe({ event }) {
    const { key, subscriber } = event

    this.unsubscribe({ event, deep: false })

    const path = JSON.stringify(event.path)
    if (path !== key) {
      this.SubscriberNestedMap.set([subscriber, key], event)
      this.PointerMultiMap.set(key, event)
    }

    this.SubscriberNestedMap.set([subscriber, path], event)
    this.PointerMultiMap.set(path, event)
  }

  unsubscribe({ event, subscriber, key, deep = true }) {
    if (event == null) {
      event = this.SubscriberNestedMap.get([subscriber, key])
    } else {
      subscriber = event.subscriber
      key = event.key
    }

    if (event instanceof TreeEvent) {
      const path = JSON.stringify(event.path)
      if (path !== key) {
        this.SubscriberNestedMap.delete([subscriber, key])
        this.PointerMultiMap.delete(key, event)
      }

      this.SubscriberNestedMap.delete([subscriber, path])
      this.PointerMultiMap.delete(path, event)

      if (deep && event.children != null) {
        event.children.forEach(child => {
          this.unsubscribe({ event: child })
        })
      }
    }
  }
}

class MultiMap {
  constructor() {
    this.map = new Map()
  }

  get(key) {
    return this.map.get(key)
  }

  set(key, value) {
    let set

    if (this.map.has(key)) {
      set = this.map.get(key)
    } else {
      set = new Set()
      this.map.set(key, set)
    }

    set.add(value)

    return this
  }

  delete(key, value) {
    if (this.map.has(key)) {
      const set = this.map.get(key)

      set.delete(value)

      if (set.size === 0) {
        this.map.delete(key)
      }

      return true
    }

    return false
  }

  clear() {
    this.map.clear()
  }

  get size() {
    return this.map.size
  }
}

class NestedMap {
  constructor() {
    this.map = new Map()
  }

  get(keys = []) {
    let map = this.map

    for (let key of keys) {
      if (map.has(key)) {
        map = map.get(key)
      } else {
        return
      }
    }

    return map
  }

  set(keys = [], value) {
    let map = this.map

    for (let key of keys.slice(0, -1)) {
      if (!map.has(key)) {
        map.set(key, new Map())
      }

      map = map.get(key)
    }

    map.set(keys[keys.length - 1], value)

    return this
  }

  delete(keys = []) {
    let map = this.map

    for (let key of keys.slice(0, -1)) {
      if (map.has(key)) {
        map = map.get(key)
      } else {
        return false
      }
    }

    map.delete(keys[keys.length - 1])

    for (let i = keys.length - 2; i >= 0; i--) {
      let key = keys[i]
      map = this.get(keys.slice(0, i))

      if (map.get(key).size === 0) {
        map.delete(key)
      }
    }

    return true
  }

  get size() {
    return this.map.size
  }
}

export default TreeEvents
