/**
  * _permit
  *
  * Given a path and zero or permission roles,
  * return whether the path is authorized.
  *
  * Rules: for any given value, the algorithm is:
  *   1. Start at the most specific node of a role (permission tree).
  *   2. If that node has a boolean value, return the value;
  *   3. If that node has a number value, it represents an expiration date,
  *      return `Date.now() - node > 0`;
  *   4. Else move on to less specific node in role (goto 1).
  *   5. If any permission role returns `true`, return `true`;
  *   6. Else return `false`.
  */

import _isNumber from 'lodash-es/isNumber'

import _toJsonPointer from './toJsonPointer'

/**
  * @arg {Array} [path=[]] - A path to a node on the state tree
  * @arg {Array} [permissions=[]] - Zero or more roles.
  *
  * @returns {Boolean} - Whether the path is accessible with the given roles
  */
const _permit = function(pathOrPointer = [], roles) {
  roles = [].concat(roles)
  for (let role of roles) {
    let path = pathOrPointer.slice() // FIXME: allow pointers
    path.push('*')

    while (path.length > 0) {
      let pointer = _toJsonPointer(path)
      let permission = pointer.get(role)

      if (_isNumber(permission)) {
        permission = Date.now() - permission > 0
      }

      if (permission === true) {
        return true
      }

      if (permission === false) {
        break
      }

      if (path[path.length - 1] !== '*') {
        path[path.length - 1] = '*'
      } else {
        path.pop()
      }
    }
  }

  return false
}

export default _permit
