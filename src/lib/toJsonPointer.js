// Dependencies
import JsonPointer from 'json-ptr'
import _isArray from 'lodash-es/isArray'
import _isString from 'lodash-es/isString'
import _get from 'lodash-es/get'

// Function
const _toJsonPointer = function(pointerOrPath) {
  if (pointerOrPath instanceof JsonPointer) {
    return pointerOrPath
  }

  if (_isArray(pointerOrPath)) {
    // pointerOrPath = JsonPointer.encodePointer(pointerOrPath)
    if (pointerOrPath.length > 0) {
      pointerOrPath = '/' + pointerOrPath.join('/')
    } else {
      pointerOrPath = ''
    }
  }

  if (_isString(pointerOrPath)) {
    let path

    if (pointerOrPath.length > 1) {
      path = pointerOrPath.slice(1).split('/')
    } else {
      path = []
    }

    return {
      pointer: pointerOrPath,
      path,
      get: target => _get(target, path, undefined)
    }
  }

  throw TypeError
}

export default _toJsonPointer
