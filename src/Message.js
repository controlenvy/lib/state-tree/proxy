/* eslint-disable no-unused-vars */

// Constants
const DELETE = 'd'
const IMMEDIATE = 'i'
const MERGE = 'm'
const PATH = 'p'
const DIFF = 'q'
const READ = 'r'
const SUBSCRIBE = 's'
const UNSUBSCRIBE = 'u'
const VALUE = 'v'
const WRITE = 'w'

const keys = {
  c: 'CHANNEL',
  d: 'DELETE',
  i: 'IMMEDIATE',
  m: 'MERGE',
  p: 'PATH',
  q: 'DIFF',
  r: 'READ',
  s: 'SUBSCRIBE',
  u: 'UNSUBSCRIBE',
  v: 'VALUE',
  w: 'WRITE'
}

// Message
class Message {
  constructor(args) {
    Object.assign(this, args)
  }

  static errors(message) {
    let errors = []

    switch (message.o) {
      case WRITE:
      case MERGE:
        if (!message.hasOwnProperty(VALUE)) {
          errors.push(`operator "${message.o}" requires value "${VALUE}"`)
        }
      case DELETE:
      case READ:
      case SUBSCRIBE:
      case UNSUBSCRIBE:
        if (!message.hasOwnProperty('p')) {
          errors.push(`operator "${message.o} requires path "${PATH}"`)
        }
        break
    }
  }
}

export default Message
