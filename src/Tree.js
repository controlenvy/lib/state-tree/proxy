// Dependencies
import _each from 'lodash-es/each'
import _isPlainObject from 'lodash-es/isPlainObject'

import { mix } from 'mixwith/src/mixwith'
import TreeBase from './TreeBase'
import TreeEvents from './TreeEvents'
import TreeHistory from './TreeHistory'
import TreeRBAC from './TreeRBAC'

/**
  * Tree
  */
class Tree extends mix(TreeBase).with(TreeEvents, TreeHistory, TreeRBAC) {
  constructor(args) {
    args = args != null ? args : {}

    // Maintain one copy of each root tree
    if (args.parent == null) {
      const existing = Tree.forest.get(args.namespace)
      if (existing instanceof Tree) {
        return existing
      }
    }

    super(args)

    this.constructor.forest.set(this._namespace, this)
  }

  static toString() {
    return '[class Tree]'
  }

  toString() {
    return '[object Tree]'
  }

  init(data) {
    if (_isPlainObject(data)) {
      _each(data, (value, key) => {
        this[key] = value
      })
    }
  }

  onchange(key, next, prev, once) {
    super.onchange(key, next, prev, once)
  }
}

Tree.forest = new Map()

export default Tree
