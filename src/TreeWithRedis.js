// Dependencies
import _each from 'lodash-es/each'
import _isPlainObject from 'lodash-es/isPlainObject'

import Redis from 'redis'

import { mix } from 'mixwith/src/mixwith'
import TreeBase from './TreeBase'
import TreeEvents from './TreeEvents'
import TreeHistory from './TreeHistory'
import TreeRBAC from './TreeRBAC'
import TreeRedis from './TreeRedis'

/**
  * Tree
  */
class Tree extends mix(TreeBase).with(
  TreeEvents,
  TreeHistory,
  TreeRBAC,
  TreeRedis
) {
  constructor(args) {
    args = args != null ? args : {}

    // Maintain one copy of each root tree
    if (args.parent == null) {
      const existing = Tree.forest.get(args.namespace)
      if (existing instanceof Tree) {
        return existing
      }
    }

    super(args)
  }

  static toString() {
    return '[class TreeWithRedis]'
  }

  toString() {
    return '[object TreeWithRedis]'
  }

  init(data) {
    if (_isPlainObject(data)) {
      _each(data, (value, key) => {
        this[key] = value
      })
    }
  }

  onchange(key, next, prev, once) {
    super.onchange(key, next, prev, once)
  }
}

Tree.forest = new Map()

Tree.forest.init = function(done) {
  const redis = Redis.createClient()

  Tree.forest.clear()

  const loading = new Set()
  redis.keys('*', function(error, replies) {
    if (error != null) {
      done(error)
      return
    }

    replies.forEach(function(namespace) {
      redis.type(namespace, (error, reply) => {
        if (error != null) {
          return
        }

        if (reply !== 'hash') {
          return
        }

        loading.add(namespace)

        new Tree({
          namespace,
          done: function() {
            loading.delete(namespace)

            if (loading.size === 0) {
              redis.quit()
              done()
            }
          }
        })
      })
    })
  })
}

Tree.quit = function() {
  const treeIterator = Tree.forest.entries()
  let done = false
  while (!done) {
    const node = treeIterator.next()
    if (node.done) {
      done = true
      break
    }

    if (node.value._redis) {
      try {
        node.value._redis.quit()
        done = true
      } catch (e) {} // eslint-disable-line no-empty
    }
  }
}

export default Tree
