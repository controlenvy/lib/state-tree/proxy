import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import json from 'rollup-plugin-json'

const camelCase = str => {
  return str.replace(/(?:-[a-z])/g, match => {
    return match[1].toUpperCase()
  })
}

const pkg = require('../package.json')

export default {
  entry: 'src/Tree.js',
  dest: 'dist/Tree.js',
  format: 'umd',
  moduleName: camelCase(pkg.name),
  plugins: [json(), resolve(), commonjs()]
}
