// Test Setup
import chai, { expect } from 'chai'
import sinon from 'sinon' // eslint-disable-line
import sinonChai from 'sinon-chai'
chai.use(sinonChai)

// Test Subject
import { _permit } from '../..'

// Fixtures
const ROLES = {
  root: {
    '*': true
  },
  admin: {
    setup: {
      '*': true
    },
    components: {
      '*': true
    }
  },
  user: {
    setup: {
      rooms: {
        1: true,
        2: true,
        3: true
      }
    },
    components: {
      1: true,
      2: true,
      3: true
    }
  },
  guest1: {
    setup: {
      rooms: {
        1: true
      }
    },
    components: {
      1: true
    }
  },
  guest2: {
    setup: {
      rooms: {
        2: true
      }
    },
    components: {
      2: true
    }
  }
}

// Tests
describe('_permit', () => {
  describe('roles', () => {
    describe('root', () => {
      const role = ROLES.root

      const TESTS = [
        {
          path: [],
          result: true
        },
        {
          path: ['setup'],
          result: true
        },
        {
          path: ['setup', 'rooms'],
          result: true
        },
        {
          path: ['setup', 'rooms', '1'],
          result: true
        },
        {
          path: ['setup', 'rooms', '2'],
          result: true
        },
        {
          path: ['setup', 'rooms', '3'],
          result: true
        },
        {
          path: ['components'],
          result: true
        },
        {
          path: ['components', '1'],
          result: true
        },
        {
          path: ['components', '2'],
          result: true
        },
        {
          path: ['components', '3'],
          result: true
        }
      ]

      for (let test of TESTS) {
        it(`should ${test.result
          ? 'permit'
          : 'deny'} access to ${JSON.stringify(test.path)}`, () => {
          const permitted = _permit(test.path, role)

          expect(permitted).to.be.equal(test.result)
        })
      }
    })

    describe('admin', () => {
      const role = ROLES.admin

      const TESTS = [
        {
          path: [],
          result: false
        },
        {
          path: ['setup'],
          result: true
        },
        {
          path: ['setup', 'rooms'],
          result: true
        },
        {
          path: ['setup', 'rooms', '1'],
          result: true
        },
        {
          path: ['setup', 'rooms', '2'],
          result: true
        },
        {
          path: ['setup', 'rooms', '3'],
          result: true
        },
        {
          path: ['components'],
          result: true
        },
        {
          path: ['components', '1'],
          result: true
        },
        {
          path: ['components', '2'],
          result: true
        },
        {
          path: ['components', '3'],
          result: true
        }
      ]

      for (let test of TESTS) {
        it(`should ${test.result
          ? 'permit'
          : 'deny'} access to ${JSON.stringify(test.path)}`, () => {
          const permitted = _permit(test.path, role)

          expect(permitted).to.be.equal(test.result)
        })
      }
    })

    describe('user', () => {
      const role = ROLES.user

      const TESTS = [
        {
          path: [],
          result: false
        },
        {
          path: ['setup'],
          result: false
        },
        {
          path: ['setup', 'rooms'],
          result: false
        },
        {
          path: ['setup', 'rooms', '1'],
          result: true
        },
        {
          path: ['setup', 'rooms', '2'],
          result: true
        },
        {
          path: ['setup', 'rooms', '3'],
          result: true
        },
        {
          path: ['components'],
          result: false
        },
        {
          path: ['components', '1'],
          result: true
        },
        {
          path: ['components', '2'],
          result: true
        },
        {
          path: ['components', '3'],
          result: true
        }
      ]

      for (let test of TESTS) {
        it(`should ${test.result
          ? 'permit'
          : 'deny'} access to ${JSON.stringify(test.path)}`, () => {
          const permitted = _permit(test.path, role)

          expect(permitted).to.be.equal(test.result)
        })
      }
    })

    describe('guest 1', () => {
      const role = ROLES.guest1

      const TESTS = [
        {
          path: [],
          result: false
        },
        {
          path: ['setup'],
          result: false
        },
        {
          path: ['setup', 'rooms'],
          result: false
        },
        {
          path: ['setup', 'rooms', '1'],
          result: true
        },
        {
          path: ['setup', 'rooms', '2'],
          result: false
        },
        {
          path: ['setup', 'rooms', '3'],
          result: false
        },
        {
          path: ['components'],
          result: false
        },
        {
          path: ['components', '1'],
          result: true
        },
        {
          path: ['components', '2'],
          result: false
        },
        {
          path: ['components', '3'],
          result: false
        }
      ]

      for (let test of TESTS) {
        it(`should ${test.result
          ? 'permit'
          : 'deny'} access to ${JSON.stringify(test.path)}`, () => {
          const permitted = _permit(test.path, role)

          expect(permitted).to.be.equal(test.result)
        })
      }
    })

    describe('guest 2', () => {
      const role = ROLES.guest2

      const TESTS = [
        {
          path: [],
          result: false
        },
        {
          path: ['setup'],
          result: false
        },
        {
          path: ['setup', 'rooms'],
          result: false
        },
        {
          path: ['setup', 'rooms', '1'],
          result: false
        },
        {
          path: ['setup', 'rooms', '2'],
          result: true
        },
        {
          path: ['setup', 'rooms', '3'],
          result: false
        },
        {
          path: ['components'],
          result: false
        },
        {
          path: ['components', '1'],
          result: false
        },
        {
          path: ['components', '2'],
          result: true
        },
        {
          path: ['components', '3'],
          result: false
        }
      ]

      for (let test of TESTS) {
        it(`should ${test.result
          ? 'permit'
          : 'deny'} access to ${JSON.stringify(test.path)}`, () => {
          const permitted = _permit(test.path, role)

          expect(permitted).to.be.equal(test.result)
        })
      }
    })

    describe('guest 1 and 2', () => {
      const TESTS = [
        {
          path: [],
          result: false
        },
        {
          path: ['setup'],
          result: false
        },
        {
          path: ['setup', 'rooms'],
          result: false
        },
        {
          path: ['setup', 'rooms', '1'],
          result: true
        },
        {
          path: ['setup', 'rooms', '2'],
          result: true
        },
        {
          path: ['setup', 'rooms', '3'],
          result: false
        },
        {
          path: ['components'],
          result: false
        },
        {
          path: ['components', '1'],
          result: true
        },
        {
          path: ['components', '2'],
          result: true
        },
        {
          path: ['components', '3'],
          result: false
        }
      ]

      for (let test of TESTS) {
        it(`should ${test.result
          ? 'permit'
          : 'deny'} access to ${JSON.stringify(test.path)}`, () => {
          const permitted = _permit(test.path, [ROLES.guest1, ROLES.guest2])

          expect(permitted).to.be.equal(test.result)
        })
      }
    })

    describe('none', () => {
      const TESTS = [
        {
          path: [],
          result: false
        },
        {
          path: ['setup'],
          result: false
        },
        {
          path: ['setup', 'rooms'],
          result: false
        },
        {
          path: ['setup', 'rooms', '1'],
          result: false
        },
        {
          path: ['setup', 'rooms', '2'],
          result: false
        },
        {
          path: ['setup', 'rooms', '3'],
          result: false
        },
        {
          path: ['components'],
          result: false
        },
        {
          path: ['components', '1'],
          result: false
        },
        {
          path: ['components', '2'],
          result: false
        },
        {
          path: ['components', '3'],
          result: false
        }
      ]

      for (let test of TESTS) {
        it(`should ${test.result
          ? 'permit'
          : 'deny'} access to ${JSON.stringify(test.path)}`, () => {
          const permitted = _permit(test.path)

          expect(permitted).to.be.equal(test.result)
        })
      }
    })
  })
})
