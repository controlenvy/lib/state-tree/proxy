// Test Setup
import chai, { expect } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
chai.use(sinonChai)

// Test Subject
import { Tree } from '..'

// Dependencies
import { _toJsonPointer } from '..'

// Fixtures
const PRIMITIVES_ALLOWED = [
  {
    type: 'string',
    value: ''
  },
  {
    type: 'string',
    value: 'Hello, World!'
  },
  {
    type: 'number (finite)',
    value: 0
  },
  {
    type: 'number (finite)',
    value: 1
  },
  {
    type: 'number (finite)',
    value: -1
  },
  {
    type: 'number (finite)',
    value: Number.MAX_SAFE_INTEGER
  },
  {
    type: 'number (finite)',
    value: Number.MIN_SAFE_INTEGER
  },
  {
    type: 'boolean',
    value: true
  },
  {
    type: 'boolean',
    value: false
  },
  {
    type: 'array',
    value: []
  },
  {
    type: 'null',
    value: null
  },
  {
    type: 'undefined',
    value: undefined
  }
]

const PRIMITIVES_DENIED = [
  {
    type: 'number (infinite)',
    value: Infinity
  },
  {
    type: 'number (infinite)',
    value: -Infinity
  },
  {
    type: 'number (not a number)',
    value: NaN
  }
]

// Tests
describe('Tree', () => {
  beforeEach(() => {
    Tree.forest.clear()
  })

  describe.skip('Benchmark', () => {
    return
    const SIZE = 1e3
    const OBJECT = {
      _id: '59053e1c4558e231de881b28',
      index: 0,
      guid: 'a80a4a64-df3f-4d3b-9af7-fa9578821466',
      isActive: true,
      balance: '$2,949.55',
      picture: 'http://placehold.it/32x32',
      age: 35,
      eyeColor: 'green',
      name: 'Big Berta',
      company: 'ORBIXTAR',
      email: 'berta.welch@orbixtar.tv',
      phone: '+1 (835) 509-2676',
      address: '632 Harbor Lane, Bedias, Connecticut, 9613',
      about:
        'Laborum adipisicing dolor excepteur fugiat qui fugiat. Sit fugiat exercitation ad officia quis culpa id aute qui in enim nisi nisi. Sint consectetur non occaecat ea sint non voluptate amet. Irure deserunt labore fugiat pariatur officia magna dolore cupidatat exercitation culpa ex in exercitation. Officia minim laborum cillum anim minim incididunt sit excepteur ex excepteur aliquip laborum.',
      registered: 'Monday, September 1, 2014 10:44 AM',
      latitude: '-67.71839',
      longitude: '134.983573',
      greeting: 'Hello, Berta! You have 5 unread messages.',
      favoriteFruit: 'strawberry'
    }

    let list, start, end

    list = Array(SIZE)
    gc() // garbage collect
    start = process.memoryUsage().heapUsed

    for (let i = 0; i < SIZE; i++) {
      list.push(JSON.parse(JSON.stringify(OBJECT)))
    }

    end = process.memoryUsage().heapUsed
    console.log(`Objects used ${(end - start) / 1e6} MB`)

    list = Array(SIZE)
    gc() // garbage collect
    start = process.memoryUsage().heapUsed

    for (let i = 0; i < SIZE; i++) {
      let tree = new Tree()
      tree.init(OBJECT)

      for (let j = 0; j < 0x20; j++) {
        for (let key in OBJECT) {
          let value = OBJECT[key]
          tree[key] = value
        }
      }

      list.push(tree)
    }

    end = process.memoryUsage().heapUsed
    console.log(`Trees used ${(end - start) / 1e6} MB`)
  })

  describe('Base', () => {
    describe('tree = new Tree()', () => {
      const tree = new Tree()

      it('is an instance of Tree', () => {
        expect(tree).to.be.an.instanceof(Tree)
      })

      it('has a blank namespace', () => {
        expect(tree._namespace).to.equal('')
      })

      it('has a parent equal to null', () => {
        expect(tree._parent).to.be.null
      })

      it('has a key equal to null', () => {
        expect(tree._key).to.be.null
      })

      it('has a path equal to []', () => {
        expect(tree._path).to.eql([])
      })

      it('has a root equal to itself', () => {
        expect(tree._root).to.equal(tree)
      })

      PRIMITIVES_ALLOWED.forEach(({ type, value }) => {
        describe(`tree.a = ${JSON.stringify(value)} // ${type}`, () => {
          const tree = new Tree()
          tree.a = value

          it('is not an instance of Tree', () => {
            expect(tree.a).not.to.be.instanceof(Tree)
          })

          it(`equals ${JSON.stringify(value)}`, () => {
            expect(tree.a).to.equal(value)
          })

          if (value === undefined) {
            it('has been deleted', () => {
              expect(tree).not.to.have.ownProperty('a')
            })
          }
        })
      })

      describe('tree.a = true; tree.a = true // twice', () => {
        const tree = new Tree()

        before(() => {
          sinon.spy(tree, 'onchange')
        })

        after(() => {
          tree.onchange.restore()
        })

        it('does not call the "onchange" callback', () => {
          tree.a = true
          tree.a = true

          expect(tree.onchange.callCount).to.equal(1)
        })
      })

      describe('Tree.forest', () => {
        it('is a map', () => {
          expect(Tree.forest).to.be.instanceof(Map)
        })

        it('tracks root trees', () => {
          const trees = []
          for (let i = 0; i < 32; i++) {
            trees.push(new Tree({ path: `/trees/${i}` }))
          }

          trees.forEach(tree => {
            expect(Tree.forest.has(tree._pointer)).to.be.true
          })
        })

        it('does not track child trees', () => {
          const trees = []
          for (let i = 0; i < 32; i++) {
            let tree = new Tree({ namespace: `/trees/${i}` })
            tree.init({ a: { b: true } })

            trees.push(tree)
          }

          trees.forEach(tree => {
            expect(Tree.forest.has(`${tree._namespace}/a`)).to.be.false
            expect(Tree.forest.has(`${tree._namespace}/a/b`)).to.be.false
          })
        })

        it('maintains a single instance of each root tree', () => {
          const first = new Tree({ namespace: '/existing' })
          const second = new Tree({ namespace: '/existing' })

          expect(first).to.equal(second)
        })
      })

      describe('tree.a = {}', () => {
        const tree = new Tree()
        tree.init({ a: {} })

        it('is an instance of Tree', () => {
          expect(tree.a).to.be.an.instanceof(Tree)
        })

        it('has a parent equal to tree', () => {
          expect(tree.a._parent).to.eql(tree) // FIXME: not `equal`, possiby due to Proxy?
        })

        it('has a key equal to "a"', () => {
          expect(tree.a._key).to.equal('a')
        })

        it('has a path equal to ["a"]', () => {
          expect(tree.a._path).to.eql(['a'])
        })

        it('has a root equal to tree', () => {
          expect(tree.a._root).to.equal(tree)
        })
      })

      describe('tree.a = { b: { c: {} } } // deeply nested', () => {
        describe('tree.a', () => {
          const tree = new Tree()
          tree.init({ a: {} })

          it('is an instance of Tree', () => {
            expect(tree.a).to.be.an.instanceof(Tree)
          })

          it('has a parent equal to tree', () => {
            expect(tree.a._parent).to.eql(tree) // FIXME: not `equal`, possiby due to Proxy?
          })

          it('has a key equal to "a"', () => {
            expect(tree.a._key).to.equal('a')
          })

          it('has a path equal to ["a"]', () => {
            expect(tree.a._path).to.eql(['a'])
          })

          it('has a root equal to tree', () => {
            expect(tree.a._root).to.equal(tree)
          })
        })

        describe('tree.a.b', () => {
          const tree = new Tree()
          tree.a = { b: { c: {} } }

          it('is an instance of Tree', () => {
            expect(tree.a.b).to.be.an.instanceof(Tree)
          })

          it('has a parent equal to tree.a', () => {
            expect(tree.a.b._parent).to.eql(tree.a) // FIXME: not `equal`, possiby due to Proxy?
          })

          it('has a key equal to "b"', () => {
            expect(tree.a.b._key).to.equal('b')
          })

          it('has a path equal to ["a", "b"]', () => {
            expect(tree.a.b._path).to.eql(['a', 'b'])
          })

          it('has a root equal to tree', () => {
            expect(tree.a._root).to.equal(tree)
          })
        })

        describe('tree.a.b.c', () => {
          const tree = new Tree()
          tree.a = { b: { c: {} } }

          it('is an instance of Tree', () => {
            expect(tree.a.b.c).to.be.an.instanceof(Tree)
          })

          it('has a parent equal to tree.a', () => {
            expect(tree.a.b.c._parent).to.eql(tree.a.b) // FIXME: not `equal`, possiby due to Proxy?
          })

          it('has a key equal to "c"', () => {
            expect(tree.a.b.c._key).to.equal('c')
          })

          it('has a path equal to ["a", "b", "c"]', () => {
            expect(tree.a.b.c._path).to.eql(['a', 'b', 'c'])
          })

          it('has a root equal to tree', () => {
            expect(tree.a.b.c._root).to.equal(tree)
          })
        })
      })

      Object.getOwnPropertyNames(tree).forEach(property => {
        describe(`tree['${property}'] = true`, () => {
          const prev = tree[property]

          it('throws a type error', () => {
            const test = function() {
              tree[property] = true
            }
            expect(test).to.throw(TypeError)
          })

          it('remains unchanged', () => {
            expect(tree[property]).to.equal(prev)
          })
        })
      })

      describe(`tree.toString = true`, () => {
        const prev = tree.toString

        it('throws a type error', () => {
          const test = function() {
            tree.toString = true
          }
          expect(test).to.throw(TypeError)
        })

        it('remains undefined', () => {
          expect(tree.toString).to.equal(prev)
        })
      })

      describe(`tree.a = function () {}`, () => {
        it('throws a type error', () => {
          const test = function() {
            tree.a = function() {}
          }
          expect(test).to.throw(TypeError)
        })

        it('remains undefined', () => {
          expect(tree.a).to.be.undefined
        })
      })
    })

    describe('tree = new Tree({ namespace: "/a/b/c" })', () => {
      describe('tree', () => {
        const tree = new Tree({ namespace: '/a/b/c' })

        it('has a namespace equal to "/a/b/c"', () => {
          expect(tree._namespace).to.eql('/a/b/c')
        })
      })
    })

    describe('tree = new Tree({ init: { a: {} } })', () => {
      describe('tree.a', () => {
        const tree = new Tree()
        tree.init({ a: {} })

        it('is an instance of Tree', () => {
          expect(tree.a).to.be.an.instanceof(Tree)
        })

        it('has a parent equal to tree', () => {
          expect(tree.a._parent).to.eql(tree) // FIXME: not `equal`, possiby due to Proxy?
        })

        it('has a key equal to "a"', () => {
          expect(tree.a._key).to.equal('a')
        })

        it('has a path equal to ["a"]', () => {
          expect(tree.a._path).to.eql(['a'])
        })

        it('has a root equal to tree', () => {
          expect(tree.a._root).to.equal(tree)
        })
      })
    })
  })

  describe('Events', () => {
    describe('standard paths', () => {
      describe('subscribe', () => {
        describe('tree', () => {
          const tree = new Tree()

          it('has an events broker', () => {
            expect(tree).to.have.ownProperty('_events')
          })

          it('has an events broker with pointer multimap', () => {
            expect(tree._events).to.have.ownProperty('PointerMultiMap')
          })

          it('has an events broker with subscriber nested map', () => {
            expect(tree._events).to.have.ownProperty('SubscriberNestedMap')
          })

          it('has a subscribe method', () => {
            expect(tree).to.respondTo('subscribe')
          })

          it('has an unsubscribe method', () => {
            expect(tree).to.respondTo('unsubscribe')
          })
        })

        PRIMITIVES_ALLOWED.forEach(({ type, value }) => {
          const PATHS = [
            {
              left: [],
              right: ['a']
            },
            {
              left: ['a'],
              right: []
            }
          ]

          describe(`tree.a = ${JSON.stringify(value)} // ${type}`, () => {
            PATHS.forEach(({ left, right }) => {
              describe(`tree.subscribe(${JSON.stringify(left)})`, () => {
                const tree = new Tree()
                const subscriber = {}
                const callback = sinon.spy()

                tree.subscribe({ path: left, subscriber, callback })
                tree.a = value

                it(`has a subscription to ${JSON.stringify(left)}`, () => {
                  const key = JSON.stringify(left)

                  expect(
                    tree._events.SubscriberNestedMap.get([subscriber, key])
                      .callback
                  ).to.equal(callback)

                  expect(
                    tree._events.PointerMultiMap.get(key)
                  ).to.be.instanceof(Set)
                })

                it(`publishes only once`, () => {
                  expect(callback.callCount).to.equal(1)
                })

                it(`publishes subscription ${JSON.stringify(
                  left
                )} for modification to ${JSON.stringify(
                  right
                )} with ${JSON.stringify(value)}`, () => {
                  expect(callback).to.have.been.calledWith(
                    tree,
                    left,
                    right,
                    value
                  )
                })

                it('includes updates to root', () => {
                  expect(tree.a).to.equal(value)
                })
              })
            })

            PATHS.forEach(({ left, right }) => {
              describe(`tree.subscribe({ path: ${JSON.stringify(
                left
              )}, immediate: true })`, () => {
                const tree = new Tree()
                const subscriber = {}
                const callback = sinon.spy()

                tree.subscribe({
                  path: left,
                  subscriber,
                  callback,
                  immediate: true
                })
                tree.a = value

                it(`publishes exactly twice`, () => {
                  expect(callback.callCount).to.equal(2)
                })
              })
            })
          })
        })

        const PATHS = [
          {
            left: [],
            right: ['a', 'b', 'c']
          },
          {
            left: ['a'],
            right: ['b', 'c']
          },
          {
            left: ['a', 'b'],
            right: ['c']
          },
          {
            left: ['a', 'b', 'c'],
            right: []
          }
        ]

        describe(`tree.a = { b: { c: true } } // deeply nested`, () => {
          PATHS.forEach(({ left, right }) => {
            describe(`tree.subscribe(${JSON.stringify(left)})`, () => {
              const tree = new Tree()
              const subscriber = {}
              const callback = sinon.spy()

              tree.subscribe({ path: left, subscriber, callback })
              tree.a = { b: { c: true } }

              it(`has a subscription to ${JSON.stringify(left)}`, () => {
                const key = JSON.stringify(left)

                expect(
                  tree._events.SubscriberNestedMap.get([subscriber, key])
                    .callback
                ).to.equal(callback)

                expect(tree._events.PointerMultiMap.get(key)).to.be.instanceof(
                  Set
                )
              })

              it(`publishes only once`, () => {
                expect(callback.callCount).to.equal(1)
              })

              it(`publishes subscription ${JSON.stringify(
                left
              )} for modification to ${JSON.stringify(
                right
              )} with true`, () => {
                expect(callback).to.have.been.calledWith(
                  tree,
                  left,
                  right,
                  true
                )
              })
            })
          })
        })

        describe(`tree.a = { b: { c: {} } } // deeply nested`, () => {
          PATHS.forEach(({ left, right }) => {
            describe(`tree.subscribe(${JSON.stringify(left)})`, () => {
              const tree = new Tree()
              const subscriber = {}
              const callback = sinon.spy()

              tree.subscribe({ path: left, subscriber, callback })
              tree.a = { b: { c: {} } }

              it(`has a subscription to ${JSON.stringify(left)}`, () => {
                const key = JSON.stringify(left)

                expect(
                  tree._events.SubscriberNestedMap.get([subscriber, key])
                    .callback
                ).to.equal(callback)

                expect(tree._events.PointerMultiMap.get(key)).to.be.instanceof(
                  Set
                )
              })

              it(`does not publish`, () => {
                expect(callback).not.to.have.been.called
              })
            })
          })
        })

        describe(`merge with RBAC (tree.merge(...))`, () => {
          const tree = new Tree()

          const subscriber = {}
          const callback = sinon.spy()

          const left = ['a']
          const right = ['b', 'c', 'd', 'e']

          tree.a = { b: { c: { d: { e: 1 } } } }

          tree.subscribe({ path: ['a'], subscriber, callback })

          const success = tree.merge(left.concat(right), 2, [{ '*': true }])

          it(`successfully merges`, () => {
            expect(success).to.be.true
          })

          it(`publishes only once`, () => {
            expect(callback.callCount).to.equal(1)
          })

          it(`publishes subscription ${JSON.stringify(
            left
          )} for modification to ${JSON.stringify(right)} with true`, () => {
            expect(callback).to.have.been.calledWith(tree, left, right, 2)
          })
        })
      })

      describe('unsubscribe', () => {
        const PATHS = [
          {
            left: [],
            right: ['a', 'b', 'c']
          },
          {
            left: ['a'],
            right: ['b', 'c']
          },
          {
            left: ['a', 'b'],
            right: ['c']
          },
          {
            left: ['a', 'b', 'c'],
            right: []
          }
        ]

        describe(`tree.a = { b: { c: true } } // deeply nested`, () => {
          PATHS.forEach(({ left, right }) => {
            describe(`tree.unsubscribe(${JSON.stringify(left)})`, () => {
              const tree = new Tree()
              const subscriber = {}
              const callback = sinon.spy()

              tree.subscribe({ path: left, subscriber, callback })
              tree.unsubscribe({ path: left, subscriber })
              tree.a = { b: { c: true } }

              it(`does not have a subscription to ${JSON.stringify(
                left
              )}`, () => {
                const key = JSON.stringify(left)

                expect(
                  tree._events.SubscriberNestedMap.get([subscriber, key])
                ).not.to.equal(callback)

                expect(
                  tree._events.PointerMultiMap.get(key)
                ).not.to.be.instanceof(Set)
              })

              it(`does not publish`, () => {
                expect(callback.callCount).to.equal(0)
              })

              it(`deletes nested maps`, () => {
                expect(tree._events.SubscriberNestedMap.get([subscriber])).to.be
                  .undefined

                expect(tree._events.SubscriberNestedMap.size).to.be.zero
              })

              it(`deletes multimaps`, () => {
                expect(tree._events.PointerMultiMap.size).to.be.zero
              })
            })
          })

          describe(`tree.unsubscribe()`, () => {
            const tree = new Tree()
            const subscriber = {}
            const callback = sinon.spy()

            PATHS.forEach(({ left, right }) => {
              tree.subscribe({ path: left, subscriber, callback })
            })

            tree.unsubscribe({ subscriber })

            PATHS.forEach(({ left, right }) => {
              it(`does not have a subscription to ${JSON.stringify(
                left
              )}`, () => {
                const key = JSON.stringify(left)

                expect(
                  tree._events.SubscriberNestedMap.get([subscriber, key])
                ).not.to.equal(callback)

                expect(
                  tree._events.PointerMultiMap.get(key)
                ).not.to.be.instanceof(Set)
              })
            })

            tree.a = { b: { c: true } }

            it(`does not publish`, () => {
              PATHS.forEach(({ left, right }) => {
                expect(callback.callCount).to.equal(0)
              })
            })

            it(`deletes nested maps`, () => {
              PATHS.forEach(({ left, right }) => {
                expect(tree._events.SubscriberNestedMap.get([subscriber])).to.be
                  .undefined

                expect(tree._events.SubscriberNestedMap.size).to.be.zero
              })
            })

            it(`deletes multimaps`, () => {
              PATHS.forEach(({ left, right }) => {
                expect(tree._events.PointerMultiMap.size).to.be.zero
              })
            })
          })
        })
      })
    })

    describe('nested paths', () => {
      describe('subscribe', () => {
        const PATH = ['a', ['b'], 'c']

        describe(JSON.stringify(PATH), () => {
          describe('tree.a = { 1: { c: "1" }, 2: { c: "2" } }', () => {
            const tree = new Tree()
            tree.init({ a: { 1: { c: '1' }, 2: { c: '2' } }, b: '1' })

            const subscriber = {}
            const callback = sinon.spy()

            tree.subscribe({ path: PATH, subscriber, callback })

            it(`has a subscription to ${JSON.stringify(PATH)}`, () => {
              const key = JSON.stringify(PATH)

              expect(
                tree._events.SubscriberNestedMap.get([subscriber, key]).callback
              ).to.equal(callback)

              expect(tree._events.PointerMultiMap.get(key)).to.be.instanceof(
                Set
              )
            })

            describe('tree.b = "2"', () => {
              const FLAT_PATH = ['a', '2', 'c']
              tree.b = '2'

              it(`publishes subscription ${JSON.stringify(FLAT_PATH)}`, () => {
                expect(callback).to.have.been.calledWith(
                  tree,
                  FLAT_PATH,
                  [],
                  '2'
                )
              })
            })
          })
        })

        const MULTIPLE_NESTED_PATH = ['a', ['b', 'c', ['d', 'e'], 'f'], 'g']
        describe(JSON.stringify(MULTIPLE_NESTED_PATH), () => {
          describe('tree = { a: { i: "1" }, b: { c: { h: "i" } }, d: { e: "h" } }', () => {
            const tree = new Tree()
            tree.init({
              a: { i: { g: '1' }, k: { g: '2' } },
              b: { c: { h: { f: 'i' }, j: { f: 'k' } } },
              d: { e: 'h' }
            })

            const subscriber = {}
            const callback = sinon.spy()

            tree.subscribe({ path: MULTIPLE_NESTED_PATH, subscriber, callback })

            it(`has a subscription to ${JSON.stringify(
              MULTIPLE_NESTED_PATH
            )}`, () => {
              const key = JSON.stringify(MULTIPLE_NESTED_PATH)

              expect(
                tree._events.SubscriberNestedMap.get([subscriber, key]).callback
              ).to.equal(callback)

              expect(tree._events.PointerMultiMap.get(key)).to.be.instanceof(
                Set
              )
            })

            describe('tree.b = "2"', () => {
              const FLAT_PATH = ['a', 'k', 'g']
              tree.d.e = 'j'
              tree.a.k.g = '3'
              tree.a.k.g = '4'
              tree.a.k.g = '5'
              tree.d.e = 'h'
              tree.a.i.g = '6'

              it(`publishes subscription ${JSON.stringify(FLAT_PATH)}`, () => {
                expect(callback.callCount).to.be.equal(6)
                expect(callback.args[0][3]).to.be.equal('2')
                expect(callback.args[1][3]).to.be.equal('3')
                expect(callback.args[2][3]).to.be.equal('4')
                expect(callback.args[3][3]).to.be.equal('5')
                expect(callback.args[4][3]).to.be.equal('1')
                expect(callback.args[5][3]).to.be.equal('6')
              })
            })
          })
        })
      })

      describe('unsubscribe', () => {
        const PATH = ['a', ['b'], 'c']

        describe(JSON.stringify(PATH), () => {
          describe('tree.a = { 1: { c: "1" }, 2: { c: "2" } }', () => {
            const tree = new Tree()
            tree.init({ a: { 1: { c: '1' }, 2: { c: '2' } }, b: '1' })

            const subscriber = {}
            const callback = sinon.spy()

            tree.subscribe({ path: PATH, subscriber, callback })
            tree.unsubscribe({ path: PATH, subscriber })

            describe('tree.b = "2"', () => {
              const FLAT_PATH = ['a', '2', 'c']
              tree.b = '2'

              it(`does not publish`, () => {
                expect(callback.callCount).to.equal(0)
              })

              it(`deletes nested maps`, () => {
                expect(tree._events.SubscriberNestedMap.get([subscriber])).to.be
                  .undefined

                expect(tree._events.SubscriberNestedMap.size).to.be.zero
              })

              it(`deletes multimaps`, () => {
                expect(tree._events.PointerMultiMap.size).to.be.zero
              })
            })
          })
        })
      })
    })
  })

  describe('History', () => {
    describe('tree.a = 0; ... tree.a = 512', () => {
      it('tracks up to 32 changes', () => {
        const tree = new Tree()
        const history = []

        for (let i = 0; i < 0x200 - 0x20; i++) {
          tree.a = i
        }

        for (let i = 0x200 - 0x20; i < 0x200; i++) {
          tree.a = i
          history.push(i)
        }

        expect(tree._history.a.read().values).to.deep.eql(history)
      })
    })
  })

  describe('RBAC', () => {
    const ROLES = {
      root: {
        r: {
          '*': true
        },
        w: {
          '*': true
        }
      },
      guest: {
        r: {
          systems: {
            1: {
              setup: {
                rooms: {
                  1: true
                }
              },
              components: {
                1: true
              }
            }
          }
        },
        w: {
          systems: {
            1: {
              setup: {
                rooms: {
                  1: true
                }
              },
              components: {
                1: true
              }
            }
          }
        }
      }
    }

    const DATA = {
      systems: {
        1: {
          setup: {
            rooms: {
              1: {
                name: 'Kitchen'
              },
              20: {
                name: 'Safe Room'
              }
            }
          },
          components: {
            1: {
              name: 'Refrigerator'
            },
            20: {
              name: 'Steel door'
            }
          }
        }
      }
    }

    describe('permit', () => {
      describe('as root', () => {
        it('should return all attributes', () => {
          const tree = new Tree()
          tree.init(DATA)

          const permitted = tree.permit(ROLES.root.r)

          expect(permitted).to.be.deep.equal(DATA)
        })
      })

      describe('as guest', () => {
        it('should return select attributes', () => {
          const expected = {
            systems: {
              1: {
                setup: {
                  rooms: {
                    1: {
                      name: 'Kitchen'
                    }
                  }
                },
                components: {
                  1: {
                    name: 'Refrigerator'
                  }
                }
              }
            }
          }

          const tree = new Tree()
          tree.init(DATA)

          const permitted = tree.permit(ROLES.guest.r)

          expect(permitted).to.be.deep.equal(expected)
        })
      })
    })

    describe('read', () => {
      describe('as root', () => {
        it('returns all attributes', () => {
          const tree = new Tree()
          tree.init(DATA)

          const read = tree.read([], ROLES.root.r)

          expect(read).to.be.deep.equal(DATA)
        })
      })

      describe('in sub tree', function() {
        it('returns all attributes', () => {
          const rootTree = new Tree()
          rootTree.init(DATA)

          const tree = rootTree.systems[1]
          const read = tree.read([], ROLES.root.r)

          expect(read).to.be.deep.equal(DATA.systems[1])
        })

        it('returns all attributes when initialized with path', () => {
          const tree = new Tree({ path: '/systems/1' })
          tree.init(DATA.systems[1])

          const read = tree.read([], ROLES.root.r)

          expect(read).to.be.deep.equal(DATA.systems[1])
        })
      })
    })

    describe('write', () => {
      const TESTS = [
        {
          path: [],
          value: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    2: {
                      name: 'Breakfast Room'
                    }
                  }
                }
              }
            }
          },
          expected: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    2: {
                      name: 'Breakfast Room'
                    }
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems'],
          value: {
            1: {
              setup: {
                rooms: {
                  2: {
                    name: 'Breakfast Room'
                  }
                }
              }
            }
          },
          expected: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    2: {
                      name: 'Breakfast Room'
                    }
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems', '1'],
          value: {
            setup: {
              rooms: {
                2: {
                  name: 'Breakfast Room'
                }
              }
            }
          },
          expected: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    2: {
                      name: 'Breakfast Room'
                    }
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems', '1', 'setup'],
          value: {
            rooms: {
              2: {
                name: 'Breakfast Room'
              }
            }
          },
          expected: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    2: {
                      name: 'Breakfast Room'
                    }
                  }
                },
                components: {
                  1: {
                    name: 'Refrigerator'
                  },
                  20: {
                    name: 'Steel door'
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems', '1', 'setup', 'rooms'],
          value: {
            2: {
              name: 'Breakfast Room'
            }
          },
          expected: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    2: {
                      name: 'Breakfast Room'
                    }
                  }
                },
                components: {
                  1: {
                    name: 'Refrigerator'
                  },
                  20: {
                    name: 'Steel door'
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems', '1', 'setup', 'rooms', '2'],
          value: {
            name: 'Breakfast Room'
          },
          expected: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    1: {
                      name: 'Kitchen'
                    },
                    2: {
                      name: 'Breakfast Room'
                    },
                    20: {
                      name: 'Safe Room'
                    }
                  }
                },
                components: {
                  1: {
                    name: 'Refrigerator'
                  },
                  20: {
                    name: 'Steel door'
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems', '1', 'setup', 'rooms', '2', 'name'],
          value: 'Breakfast Room',
          expected: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    1: {
                      name: 'Kitchen'
                    },
                    2: {
                      name: 'Breakfast Room'
                    },
                    20: {
                      name: 'Safe Room'
                    }
                  }
                },
                components: {
                  1: {
                    name: 'Refrigerator'
                  },
                  20: {
                    name: 'Steel door'
                  }
                }
              }
            }
          }
        }
      ]

      TESTS.forEach(({ path, value, expected }) => {
        describe('as root', () => {
          it(`writes to ${JSON.stringify(path)} with ${value}`, () => {
            const tree = new Tree()
            tree.init(DATA)

            const success = tree.write(path, value, ROLES.root.w)

            expect(success).to.be.true
          })

          it(`overwrites the tree at ${JSON.stringify(path)}`, () => {
            const tree = new Tree()
            tree.init(DATA)

            const success = tree.write(path, value, ROLES.root.w)

            expect(success).to.be.true
            expect(tree).to.be.eql(expected)
          })
        })
      })

      describe('in sub tree', () => {
        const value = {
          setup: {
            rooms: {
              2: {
                name: 'Breakfast Room'
              }
            }
          }
        }

        it('writes to the correct path', () => {
          const rootTree = new Tree()
          rootTree.init(DATA)

          const tree = rootTree.systems[1]

          const expected = {
            setup: {
              rooms: {
                2: {
                  name: 'Breakfast Room'
                }
              }
            }
          }

          tree.write([], value, ROLES.root.w)

          expect(tree).to.be.eql(expected)
        })

        it('writes to the correct path when initialized with path', () => {
          const tree = new Tree({ path: '/systems/1' })
          tree.init(DATA.systems[1])

          const expected = {
            setup: {
              rooms: {
                2: {
                  name: 'Breakfast Room'
                }
              }
            }
          }

          tree.write([], value, ROLES.root.w)

          expect(tree).to.be.eql(expected)
        })
      })
    })

    describe('merge', () => {
      const TESTS = [
        {
          path: [],
          value: {
            systems: {
              1: {
                setup: {
                  rooms: {
                    2: {
                      name: 'Breakfast Room'
                    }
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems'],
          value: {
            1: {
              setup: {
                rooms: {
                  2: {
                    name: 'Breakfast Room'
                  }
                }
              }
            }
          }
        },
        {
          path: ['systems', '1'],
          value: {
            setup: {
              rooms: {
                2: {
                  name: 'Breakfast Room'
                }
              }
            }
          }
        },
        {
          path: ['systems', '1', 'setup'],
          value: {
            rooms: {
              2: {
                name: 'Breakfast Room'
              }
            }
          }
        },
        {
          path: ['systems', '1', 'setup', 'rooms'],
          value: {
            2: {
              name: 'Breakfast Room'
            }
          }
        },
        {
          path: ['systems', '1', 'setup', 'rooms', '2'],
          value: {
            name: 'Breakfast Room'
          }
        },
        {
          path: ['systems', '1', 'setup', 'rooms', '2', 'name'],
          value: 'Breakfast Room'
        }
      ]

      const expected = {
        systems: {
          1: {
            setup: {
              rooms: {
                1: {
                  name: 'Kitchen'
                },
                2: {
                  name: 'Breakfast Room'
                },
                20: {
                  name: 'Safe Room'
                }
              }
            },
            components: {
              1: {
                name: 'Refrigerator'
              },
              20: {
                name: 'Steel door'
              }
            }
          }
        }
      }

      TESTS.forEach(({ path, value }) => {
        describe('as root', () => {
          it(`patches ${JSON.stringify(path)} with ${value}`, () => {
            const tree = new Tree()
            tree.init(DATA)

            const success = tree.merge(path, value, ROLES.root.w)

            expect(success).to.be.true
          })

          it(`merges the tree at ${JSON.stringify(path)}`, () => {
            const tree = new Tree()
            tree.init(DATA)

            tree.merge(path, value, ROLES.root.w)

            expect(tree).to.be.eql(expected)
          })
        })
      })

      describe('as sub tree', () => {
        const value = {
          setup: {
            rooms: {
              2: {
                name: 'Breakfast Room'
              }
            }
          }
        }

        it('merges at the correct path', () => {
          const rootTree = new Tree()
          rootTree.init(DATA)

          const tree = rootTree.systems[1]

          const expected = {
            setup: {
              rooms: {
                1: {
                  name: 'Kitchen'
                },
                2: {
                  name: 'Breakfast Room'
                },
                20: {
                  name: 'Safe Room'
                }
              }
            },
            components: {
              1: {
                name: 'Refrigerator'
              },
              20: {
                name: 'Steel door'
              }
            }
          }

          tree.merge([], value, ROLES.root.w)

          expect(tree).to.be.eql(expected)
        })

        it('merges at the correct path when initialized with path', () => {
          const tree = new Tree({ path: '/systems/1' })
          tree.init(DATA.systems[1])

          const expected = {
            setup: {
              rooms: {
                1: {
                  name: 'Kitchen'
                },
                2: {
                  name: 'Breakfast Room'
                },
                20: {
                  name: 'Safe Room'
                }
              }
            },
            components: {
              1: {
                name: 'Refrigerator'
              },
              20: {
                name: 'Steel door'
              }
            }
          }

          tree.merge([], value, ROLES.root.w)

          expect(tree).to.be.eql(expected)
        })
      })
    })
  })
})
