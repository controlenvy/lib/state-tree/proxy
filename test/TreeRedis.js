// Test Setup
import chai, { expect } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
chai.use(sinonChai)

import Redis from 'redis'

import _debounce from 'lodash-es/debounce'
const DEBOUNCE_TIME = 10

// Test Subject
import { TreeWithRedis as Tree } from '..'

// Fixtures
const PRIMITIVES_STORED = [
  {
    type: 'string',
    value: ''
  },
  {
    type: 'string',
    value: 'Hello, World!'
  },
  {
    type: 'number (finite)',
    value: 0
  },
  {
    type: 'number (finite)',
    value: 1
  },
  {
    type: 'number (finite)',
    value: -1
  },
  {
    type: 'number (finite)',
    value: Number.MAX_SAFE_INTEGER
  },
  {
    type: 'number (finite)',
    value: Number.MIN_SAFE_INTEGER
  },
  {
    type: 'boolean',
    value: true
  },
  {
    type: 'boolean',
    value: false
  },
  {
    type: 'null',
    value: null
  }
]

// Tests
describe('TreeRedis', () => {
  const client = Redis.createClient({
    retry_strategy: function(options) {
      if (options.error && options.error.code === 'ECONNREFUSED') {
        // End reconnecting on a specific error and flush all commands with a individual error
        return new Error('The server refused the connection')
      }
      if (options.total_retry_time > 1000 * 60 * 60) {
        // End reconnecting after a specific timeout and flush all commands with a individual error
        return new Error('Retry time exhausted')
      }
      if (options.attempt > 10) {
        // End reconnecting with built in error
        return undefined
      }

      // reconnect after
      return Math.min(options.attempt * 100, 3000)
    }
  })

  describe('read', () => {
    beforeEach(() => {
      client.flushdb()
    })

    PRIMITIVES_STORED.forEach(({ type, value }) => {
      describe(`client.hset("", "/a", ${JSON.stringify(value)})`, () => {
        beforeEach(done => {
          client.hset('', '/a', JSON.stringify(value), error => {
            done()
          })
        })

        describe('tree = new Tree()', () => {
          it(`tree.a === ${value} // ${type}`, done => {
            const tree = new Tree({
              done: function() {
                expect(this.a).to.equal(value)
                done()
              }
            })
          })
        })
      })

      describe(`client.hset("", "/a/b", ${JSON.stringify(value)})`, () => {
        beforeEach(done => {
          client.hset('', '/a/b', JSON.stringify(value), error => {
            done()
          })
        })

        describe('tree = new Tree()', () => {
          describe('tree.a', () => {
            it('is an instance of Tree', done => {
              const tree = new Tree({
                done: function() {
                  expect(this.a).to.be.instanceof(Tree)
                  done()
                }
              })
            })
          })

          describe('tree.a.b', () => {
            it(`equals ${value} (${type})`, done => {
              const tree = new Tree({
                done: function() {
                  expect(this.a.b).to.equal(value)
                  done()
                }
              })
            })
          })
        })
      })

      describe(`client.hmset("", { "/a": { "/b1": ${JSON.stringify(
        value
      )}, "/b2": ${JSON.stringify(value)} } })`, () => {
        beforeEach(done => {
          client.hmset(
            '',
            {
              '/a/b1': JSON.stringify(value),
              '/a/b2': JSON.stringify(value)
            },
            error => {
              done()
            }
          )
        })

        describe('tree = new Tree()', () => {
          describe('tree.a', () => {
            it('is an instance of Tree', done => {
              const tree = new Tree({
                done: function() {
                  expect(this.a).to.be.instanceof(Tree)
                  done()
                }
              })
            })
          })

          describe('tree.a.b1', () => {
            it(`equals ${value} (${type})`, done => {
              const tree = new Tree({
                done: function() {
                  expect(this.a.b1).to.equal(value)
                  done()
                }
              })
            })
          })

          describe('tree.a.b2', () => {
            it(`equals ${value} (${type})`, done => {
              const tree = new Tree({
                done: function() {
                  expect(this.a.b2).to.equal(value)
                  done()
                }
              })
            })
          })
        })
      })
    })
  })

  describe('write', () => {
    beforeEach(() => {
      client.flushdb()
    })

    PRIMITIVES_STORED.forEach(({ type, value }) => {
      describe(`tree = new Tree({ init: { a: ${JSON.stringify(
        value
      )} } })`, () => {
        beforeEach(done => {
          const tree = new Tree({ done })
          tree.init({ a: value })
        })

        describe('client.hget("", "/a")', () => {
          it(`equals ${value}`, done => {
            client.hget('', '/a', (error, reply) => {
              const parsed = JSON.parse(reply)
              expect(parsed).to.equal(value)
              done()
            })
          })
        })
      })

      describe(`client.hget("", "/a/b", ${JSON.stringify(value)})`, () => {
        beforeEach(done => {
          const tree = new Tree({ done })
          tree.init({ a: { b: value } })
        })

        describe('tree.a', () => {
          it('is an instance of Tree', done => {
            const tree = new Tree({
              done: function() {
                expect(this.a).to.be.instanceof(Tree)
                done()
              }
            })
          })
        })

        describe('tree.a.b', () => {
          it(`equals ${value} (${type})`, done => {
            const tree = new Tree({
              done: function() {
                expect(this.a.b).to.equal(value)
                done()
              }
            })
          })
        })
      })
    })
  })

  describe('subscribe', () => {
    beforeEach(() => {
      client.flushdb()
    })

    it('returns value at a subscribed keypath', done => {
      const role = { '*': true }
      const path = ['a']
      const expected = 'c'
      let tree = null
      const updateValue = () => {
        tree.a = expected
      }

      tree = new Tree({
        namespace: '/test/1',
        done: updateValue
      })
      tree.init({ a: 'b' })

      const callback = (root, left, right /*, value*/) => {
        const path = left.concat(right)
        expect(root.read(path, role)).to.equal(expected)
        done()
      }
      tree.subscribe({ subscriber: {}, path, callback })
    })
  })

  describe('Tree.forest', () => {
    const COUNT = 0x10

    before(done => {
      client.flushdb()

      const batch = client.batch()

      for (let i = 0; i < COUNT; i++) {
        batch.hset(`/test/${i}`, '/a', 'true')
      }

      batch.exec(error => {
        if (error != null) {
          done(error)
        } else {
          Tree.forest.init(done)
        }
      })
    })

    describe('init', () => {
      it(`populates with ${COUNT} trees`, () => {
        expect(Tree.forest.size).to.equal(COUNT)
      })

      for (let i = 0; i < COUNT; i++) {
        ;(namespace => {
          describe(`[tree "${namespace}"].a`, () => {
            it('equals true', () => {
              const tree = Tree.forest.get(namespace)

              expect(tree.a).to.be.true
            })
          })
        })(`/test/${i}`)
      }
    })
  })

  describe('Tree.forest.nested', () => {
    const COUNT = 0x4

    before(done => {
      client.flushdb()

      const batch = client.batch()

      for (let i = 0; i < COUNT; i++) {
        batch.hset(`/test/${i}`, '/a/b/c/d', 'true')
      }

      batch.exec(error => {
        if (error != null) {
          done(error)
        } else {
          Tree.forest.init(() => {
            // Simulates restarting server
            Tree.forest.init(done)
          })
        }
      })
    })

    it(`structures ${COUNT} deeply nested nodes`, () => {
      for (let i = 0; i < COUNT; i++) {
        ;(namespace => {
          describe(`[tree "${namespace}"].a.b.c.d`, () => {
            it('equals true', () => {
              const tree = Tree.forest.get(namespace)

              expect(tree).to.include.keys('a')
              expect(tree.a).to.include.keys('b')
              expect(tree.a.b).to.include.keys('c')
              expect(tree.a.b.c).to.include.keys('d')
              expect(tree.a.b.c.d).to.equal(true)
            })
          })
        })(`/test/${i}`)
      }
    })
  })
})
