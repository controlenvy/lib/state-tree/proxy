# Credits
## Wikipedia
  - [Tree (data structure)](https://en.wikipedia.org/wiki/Tree_(data_structure))
  - [Radix tree](https://en.wikipedia.org/wiki/Radix_tree)

## Stack Overflow
  - [Can I extend Proxy with an ES2015 class?](http://stackoverflow.com/questions/37714787/can-i-extend-proxy-with-an-es2015-class)
